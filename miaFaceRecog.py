#! /usr/bin/env python
#--------------------------------------------------------------------------
# Copyright 2018 Cyber-Renegade.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Name:        Object detect class and buffer
#              
# Purpose:     Classes which utilize the multi processing environment to
#              to do facial recognition 
#
# Author:      Dani Thomas
#
# Requires:    OpenCV, dlib and face_recognition_models
# Based on: 
#-------------------------------------------------------------------------
import os
import miaConfig
import sys
import numpy as np
import argparse
import dlib
import cv2
import glob
import face_recognition_models
import json
import pickle

from miaMultiProcEnv import TransformProcess

class miaFaceRecog(TransformProcess): 
   FaceEncodings=[]
   FaceLabels={"faces":[]}
   EncodingsFile = None 
   LabelFile = None
   width=None
     
   def __init__(self,encodingsFile=None,labelFile=None,width=None):
      self.num_jitters = 1
      self.detector = dlib.get_frontal_face_detector()
      self.predictor = dlib.shape_predictor(face_recognition_models.pose_predictor_model_location())
      self.face_recognition_model = face_recognition_models.face_recognition_model_location()
      self.encoder = dlib.face_recognition_model_v1(self.face_recognition_model)
      self.width=width
      if encodingsFile is not None:
          self.setupPersistance(encodingsFile, labelFile)
          
   def rect_to_bb(self,rect):
      x = rect.left()
      y = rect.top()
      w = rect.right() - x
      h = rect.bottom() - y
      return (x, y, w, h)
   
   def setupPersistance(self,encodingsFile,labelFile):
      config=miaConfig.miaConfig()
      faceDataDir=config.GetConfig('FACE_DIR')
      self.EncodingsFile=faceDataDir + encodingsFile  
      self.LabelFile=faceDataDir + labelFile
      if os.path.isfile(self.EncodingsFile):
          self.FaceEncodings = pickle.loads(open(self.EncodingsFile, "rb").read())
          self.FaceLabels = json.load(open(self.LabelFile, "rb"))
      else:
          self.createDirectory(self.EncodingsFile)
          
   def getEncodings(self,image,rects,num_jitters):
      facePredictor = self.predictor(image, rect)
      return np.array(self.encoder.compute_face_descriptor(image, facePredictor, num_jitters))
   
   def face_distance(self, encodings):
      if len(self.FaceEncodings) == 0:
          return np.empty((0))
      return np.linalg.norm(self.FaceEncodings - encodings, axis=1) 
   
   def resize(self,image, width=None, height=None, inter=cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation=inter)

    # return the resized image
    return resized
     
   def face_known(self,encodings):
      faceDistances=self.face_distance(encodings)
      faceCount=0
      for faceDistance in faceDistances:          
          if faceDistance <= 0.6:
            self.FaceLabels["faces"][faceCount]["count"]+=1          
            return self.FaceLabels["faces"][faceCount]["name"]
          faceCount=faceCount+1
          
      self.FaceEncodings.append(encodings)
      jsonFace={}
      jsonFace["name"]="face" + str(faceCount+1)
      jsonFace["count"] = 1
      self.FaceLabels["faces"].append(jsonFace)     
      return self.FaceLabels["faces"][faceCount]["name"]

   def createDirectory(self,fileFullPath):
      dirName=os.path.dirname(fileFullPath)
      if not os.path.exists(dirName):
        os.makedirs(dirName)
        print("Directory " , dirName ,  " Created ")
           
   def DetectFaces(self,image):
      image=self.resize(image,width=self.width)
      rects = self.detector(image, 1)
      if len(rects)>0:
         for rect in rects:
           facePredictor = self.predictor(image, rect)
           encodings = np.array(self.encoder.compute_face_descriptor(image, facePredictor, self.num_jitters))
           faceName=self.face_known(encodings)
           (x, y, w, h) = self.rect_to_bb(rect)
           cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)
           cv2.putText(image, faceName, (x - 10, y - 10),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
           print (faceName + " found")
      return image
      
   def run(self):
      while True:
         pItem=self.ReceiveConn.recv()
         if (pItem is None):break
         if type(pItem)==float or type(pItem)==str:
           self.SendConn.send(pItem)
         else:    
           pItem=self.DetectFaces(pItem)  
           self.SendConn.send(pItem)
      
      if self.EncodingsFile is not None:
         print(self.EncodingsFile)
         with open(self.EncodingsFile, "wb") as f:
             f.write(pickle.dumps(self.FaceEncodings)) 
         with open(self.LabelFile, "w") as f:
             json.dump(self.FaceLabels, f)
      self.SendConn.send(None)
     