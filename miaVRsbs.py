#! /usr/bin/env python
#--------------------------------------------------------------------------
# Copyright 2018 Cyber-Renegade.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Name:        VR side by side Streaming Server
#              
# Purpose:     Class which utilize the multi processing environment to
#              stream VR sbs images to the web. To use call python3 maVRsbs.py
#              and then open your browser at your port : 8888 
#
# Author:      Dani Thomas
#
# Requires:    OpenCV
# Based on: 
#-------------------------------------------------------------------------
import socket
import cv2
from miaCamera import ReadCam
from miaMultiProcEnv import ReceiveProcess, MiaEnv

class miaVRsbs(ReceiveProcess):

    def run(self):
        HOST, PORT = '', 8888
        listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listen_socket.bind((HOST, PORT))
        listen_socket.listen(1)
        
        while True:
            client_connection, client_address = listen_socket.accept()
            request = client_connection.recv(1024)
            stringRequest=request.decode()
            print(stringRequest)
            fstLine = stringRequest.split('\r\n', 1)[0]
            webAsset='.' + fstLine.split(' ')[1]
            if (webAsset=='./video_feed'):
                self.generateStream(client_connection)
                client_connection.close()
                break  
            elif (webAsset=='./favicon.ico'):
                continue        
            else:
                if webAsset=='./':
                  webAsset='./index.html'
                webData=self.readWebFile(webAsset)
                http_response = b'\r\nHTTP/1.1 200 OK\r\n\r\n'  + webData.encode() + b'\r\n'                          
                client_connection.sendall(http_response)
                client_connection.close()


    def readWebFile(self,webAsset):
        with open(webAsset, 'r') as myfile:
          data = myfile.read()
        return data
    
    def generateStream(self,client_connection):
        http_response = b'\r\nHTTP/1.1 200 OK\r\n' \
                        b'Content-Type: multipart/x-mixed-replace; boundary=frame\r\n\r\n'
        client_connection.sendall(http_response)
        while True:
            frame=self.ReceiveConn.recv()
            if frame is None: break
            elif type(frame)==float:continue
            elif type(frame)==str:continue
            ret, jpeg = cv2.imencode('.jpg', frame)
            http_response = (b'--frame\r\n' \
                             b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n')
            try:
              client_connection.sendall(http_response)
            except socket.error as e:
              print('Socket Error : broken pipe')
            except IOError as e:
              if e.errno == errno.EPIPE:
                print('EPipe error')
              else:
                print (e.value)
 
            
if __name__ == '__main__':
    
    mserv= miaVRsbs()
    readCam = ReadCam(framesize='large')
    bsPrs=MiaEnv([readCam, mserv])
    