#! /usr/bin/env python
#--------------------------------------------------------------------------
# Copyright 2018 Cyber-Renegade.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Name:        Video web class
#              
# Purpose:     Classes which utilize the multi processing environment to
#              capture video from YouTube and display using OpenCV
#              It displays a list of frame and format sizes which you can 
#              choose the most appropriate. 
#
# Author:      Dani Thomas
#
# Requires:    OpenCV, pafy and ffpyplayer if you want sound
# Based on: 
#-------------------------------------------------------------------------
import miaConfig
import cv2
import signal
from miaMultiProcEnv import ReceiveProcess, SendProcess, MiaEnv
import time
import pafy
from ffpyplayer.player import MediaPlayer
from datetime import datetime

class ReadWebVideo(SendProcess):
    terminate=False
    play=None
    mediatype=None
    cutdown=1
    fps=15.0
    
    def quitGracefully(self,signum, frame):
        self.terminate=True
        
    def __init__(self,url,mediatype='', extension='',quality='',cutdown=1):
        self.cutdown=cutdown
        self.mediatype=mediatype
        # If control c is pressed should quit everything gracefully
        signal.signal(signal.SIGINT, self.quitGracefully)
        
        vPafy = pafy.new(url)
        streams = vPafy.allstreams

        for play in streams:
          print(play)
          if play.mediatype==mediatype and play.extension==extension and play.quality==quality:
            self.play=play
    
        if self.play==None:
          print('No mediatype,extension, and quality selected')
          self.terminate=True
    
    # frames per second can be retrieved from video
    def GetFramesPerSecond(self,video):
      (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
      if int(major_ver)  < 3 :
        return video.get(cv2.cv.CV_CAP_PROP_FPS)
      else :
        return video.get(cv2.CAP_PROP_FPS)
                      
    def run(self):
        if self.terminate:
          self.SendConn.send(None)
          return       
        self.stream = cv2.VideoCapture(self.play.url)
        self.fps=self.GetFramesPerSecond(self.stream)
        self.SendConn.send(self.fps) # Send the frames per second down the pipe
        if self.mediatype=='normal':
          self.player = MediaPlayer(self.play.url)
        
        framenum=1
        
        while not(self.terminate):
          (grabbed, frame) = self.stream.read()
          if framenum % self.cutdown == 0:
            self.SendConn.send(frame)
          framenum=framenum+1 
        self.SendConn.send(None)
        self.stream.release()
        return
        
class DisplayCam(ReceiveProcess):
 
    def run(self):
      while True:
        frame=self.ReceiveConn.recv()
        if frame is None: break
        elif type(frame)==float:continue
        elif type(frame)==str:continue
        cv2.imshow('frame',frame)
        cv2.waitKey(1)
      cv2.destroyAllWindows()
      return  

        
if __name__ == '__main__':

    displayVid = DisplayCam()
    
    url = 'https://www.youtube.com/watch?v=AIR5XPWK3Vk'
    readWebV = ReadWebVideo(url,'normal','mp4','640x360',cutdown=1)
    bsPrs=MiaEnv([readWebV, displayVid])
    