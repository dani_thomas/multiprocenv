#! /usr/bin/env python
#--------------------------------------------------------------------------
# Copyright 2019 Cyber-Renegade.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Name:        Web Images class
#              
# Purpose:     Class which gets images from google 
#              This is a cutdown version of google-images-download
#              
#
# Author:      Dani Thomas
#
# Requires:    
# Based on: hardikvasa/google-images-download
#-------------------------------------------------------------------------
import urllib.request
import json
from urllib.request import Request, urlopen
from urllib.request import URLError, HTTPError
from urllib.parse import quote
import http.client
from html.parser import HTMLParser
from http.client import IncompleteRead, BadStatusLine
http.client._MAXHEADERS = 50

class GoogleImages:

    # Finding 'Next Image' from the given raw page
    def _get_next_item(self,s):
        start_line = s.find('rg_meta notranslate')
        if start_line == -1:  # If no links are found then give an error!
            end_quote = 0
            link = "no_links"
            return link, end_quote
        else:
            start_line = s.find('class="rg_meta notranslate">')
            start_object = s.find('{', start_line + 1)
            end_object = s.find('</div>', start_object + 1)
            object_raw = str(s[start_object:end_object])

        try:
            object_decode = bytes(object_raw, "utf-8").decode("unicode_escape")
            final_object = json.loads(object_decode)
        except:
            final_object = ""
           
        return final_object, end_object
        
    def Search(self,search_term):        
        url = 'https://www.google.com/search?q=' + quote(
                search_term.encode('utf-8')) + '&tbm=isch'
        return str(self.GetUrl(url))

    def GetUrl(self,url):
        headers = {}
        headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
        req = urllib.request.Request(url, headers=headers)
        try:
            with urllib.request.urlopen(req) as response:
                resp = response.read()
                return resp
        except HTTPError as e:
            print(e.code)
        except urllib.error.URLError as e:
            print(e.reason)   

    def GetImages(self,page,limit,image_format=None):
        count=0
        links = []
        while count < limit:
            object, end_content = self._get_next_item(page)
            if object=='no_links':
                break
            object = self.format_object(object)
            if image_format is not None and object['image_format'] != image_format:
                page = page[end_content:]
                continue
            links.append(object['image_link'])
            page = page[end_content:]
            count += 1
        return links
    
        #Format the object in readable format
    def format_object(self,object):
        formatted_object = {}
        formatted_object['image_format'] = object['ity']
        formatted_object['image_height'] = object['oh']
        formatted_object['image_width'] = object['ow']
        formatted_object['image_link'] = object['ou']
        formatted_object['image_description'] = object['pt']
        formatted_object['image_host'] = object['rh']
        formatted_object['image_source'] = object['ru']
        formatted_object['image_thumbnail_url'] = object['tu']
        return formatted_object

    def download_image(self,image_url, image_path):
        resp= self.GetUrl(image_url)
        try:
            output_file = open(image_path, 'wb')
            output_file.write(resp)
            output_file.close()

        except OSError as e:
            print(e.description)
            
def main():
    googleImages = GoogleImages()
    page = googleImages.Search('angelina jolie')
    imageUrls = googleImages.GetImages(page,20,'jpg')
    count=1
    for url in imageUrls:
        print(url)
        googleImages.download_image(url,'angelinajolie' + str(count) + '.jpg')
        count += 1
        
if __name__ == "__main__":
    main()
