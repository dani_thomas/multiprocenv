#! /usr/bin/env python
#--------------------------------------------------------------------------
# Copyright 2018 Cyber-Renegade.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Name:        Web Image classes
#              
# Purpose:     Class which utilize the multi processing environment to
#              read images from the web
#
# Author:      Dani Thomas
#
# Requires:    OpenCV
# Based on: 
#-------------------------------------------------------------------------
import miaConfig
import miaImages
import urllib.request
from urllib.request import URLError, HTTPError
import numpy as np
from datetime import datetime
import os
import cv2
import signal
import sys
from miaMultiProcEnv import ReceiveProcess, SendProcess, MiaEnv

class ReadImageWeb(SendProcess):
    terminate=False
    
    def quitGracefully(self,signum, frame):
        self.terminate=True
        
    def __init__(self,imageUrlArr,width=None):
      # If control c is pressed should quit everything gracefully
      signal.signal(signal.SIGINT, self.quitGracefully)
      self.imageUrlArr = imageUrlArr
      self.width=width
    
    def GetUrl(self,url):
        headers = {}
        headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
        req = urllib.request.Request(url, headers=headers)
        try:
            with urllib.request.urlopen(req) as response:
                image = np.asarray(bytearray(response.read()), dtype="uint8")
                return image
        except HTTPError as e:
            print("HTTPError " + str(e.code))
        except urllib.error.URLError as e:
            print(e.reason) 
            
    def resize(self,image):
      (h, w) = image.shape[:2]
      r = self.width / float(w)
      dim = (self.width, int(h * r))
      # resize the image
      return cv2.resize(image, dim, interpolation=cv2.INTER_AREA)  
                     
    def run(self):
      for Url in self.imageUrlArr:
          if self.terminate: break        
          image = self.GetUrl(Url)
          if image is not None:
            try:
                image = cv2.imdecode(image, cv2.IMREAD_COLOR)
                if self.width is not None:
                  image=self.resize(image)
                self.SendConn.send(image) # Send the frames per second down the pipe
            except:
                print("Oops!",sys.exc_info()[0],"occured.")
                print(image)
      self.SendConn.send(None) # Sending None will terminate the other processes
      return
        
if __name__ == '__main__':        
    rdImage=ReadImageWeb(['https://danicymru.files.wordpress.com/2016/10/img_20141217_175205.jpg'])
    displayImage=miaImages.DisplayImage(width=500)
    bsPrs=MiaEnv([rdImage, displayImage])
